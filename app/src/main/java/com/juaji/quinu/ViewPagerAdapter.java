package com.juaji.quinu;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que contiene a los controladores de los diferentes fragmentos en una lista.
 * Cabe aclarar que cada vista (fragmento) tiene un controlador; Esta clase será
 * usada por el ViewPager como un intermediario entre los fragmentos y los tabs del TabLayout.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    // ----------------------- Atributos ---------------------------

    // Lista de controladores de los fragmentos
    private List<ApiFragment> fragmentList;

    // ------------------------- Métodos ---------------------------

    /**
     * Constructor (requerido al haber heredado de FragmentPagerAdapter)
     * @param fm No lo se
     * @param behavior No lo se
     */
    public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
        fragmentList = new ArrayList<>();
    }
    /**
     * Método que permite obtener un controlador de fragmento por su índice.
     * @param position Índice del controlador a obtener.
     * @return Objeto de tipo Fragment
     */

    // TODO Revisar el uso de este método, ya que posiblemente requiera de un casteo según el controlador de fragmento a obtener

    @NonNull
    @Override
    public ApiFragment getItem(int position) {
        return fragmentList.get(position);
    }
    /**
     * Método que permite obtener el número de controladores almacenados.
     * @return Número de items de la lista de controladores.
     */
    @Override
    public int getCount() {
        return fragmentList.size();
    }

    /**
     * Meétodo que permite agregar un controlador de fragmento a la lista.
     * @param f Objeto de tipo Fragment
     */
    public void addFragment(ApiFragment f){
        fragmentList.add(f);
    }
}
