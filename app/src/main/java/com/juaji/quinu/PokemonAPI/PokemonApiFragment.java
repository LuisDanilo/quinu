package com.juaji.quinu.PokemonAPI;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.juaji.quinu.ApiFragment;
import com.juaji.quinu.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Clase que modela el controlador de la vista pokemon_api_fragment.xml
 */
public class PokemonApiFragment extends ApiFragment {

    private PokemonRecyclerViewAdapter recyclerViewAdapter;

    // -------------- Métodos --------------------

    /**
     * Constructor de la clase.
     * @param baseUrl String con la url base de la API a consumir.
     * @param complementaryUrl String con la url complementaria de la API a consumir.
     */
    public PokemonApiFragment(String baseUrl, String complementaryUrl) {
        super(baseUrl, complementaryUrl);
    }

    /**
     * Método que realiza la obtención de datos del API requerida.
     * A pesar de que cada fragmento hace uso de una instancia de ApiService, esto no impide
     * que desde este fragmento se pueda obtener datos de una API que no corresponde al mismo.
     */
    private void getData() {
        // Este fragmento es para la API de pokemon, por tanto se usa el método respectivo
        Call<PokemonApiResponseHandler> data = service.getPokemonData(complementaryUrl);
        data.enqueue(new Callback<PokemonApiResponseHandler>() {
            @Override
            public void onResponse(Call<PokemonApiResponseHandler> call, Response<PokemonApiResponseHandler> response) {
                if (response.isSuccessful()) {
                    PokemonApiResponseHandler body = response.body();
                    recyclerViewAdapter.addToDataset(body.getResults());
                }
            }

            @Override
            public void onFailure(Call<PokemonApiResponseHandler> call, Throwable t) {

            }
        });
    }

    /**
     * Método sobreescrito para establecer la vista asociada a este controlador
     * @param inflater No lo se
     * @param container No lo se
     * @param savedInstanceState No lo se
     * @return La vista asociada a este controlador
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.pokemon_api_fragment, container, false);
        recyclerView = rootView.findViewById(R.id.pokemonRecycler);
        recyclerViewAdapter = new PokemonRecyclerViewAdapter(getActivity());
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        getData();
        return rootView;
    }
}
