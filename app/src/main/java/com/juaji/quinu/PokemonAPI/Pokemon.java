package com.juaji.quinu.PokemonAPI;

class Pokemon {

    // -------- Atributos ----------
    private String name;
    private String url;
    private String number;

    // -------- Métodos ------------


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNumber() {
        String[] temp = url.split("/");
        return temp[temp.length-1];
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
