package com.juaji.quinu.PokemonAPI;

import java.util.ArrayList;

/**
 * Clase con la cual son contenidos los datos que son solicitados a la API.
 * A partir de una instancia de esta clase se puede manipular dichos datos.
 * Nota: Para que la obtencón de datos sea exitosa esta clase debe tener atributos con el mismo
 * nombre y tipo de aquellos datos que se desea obtener del JSON entregado por la API.
 */
public class PokemonApiResponseHandler {

    // ---------- Atributos ------------
    private ArrayList<Pokemon> results;

    // ---------- Métodos --------------
    public ArrayList<Pokemon> getResults() {
        return results;
    }

    public void setResults(ArrayList<Pokemon> results) {
        this.results = results;
    }
}
