package com.juaji.quinu.PokemonAPI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.juaji.quinu.R;

import java.util.ArrayList;

public class PokemonRecyclerViewAdapter extends RecyclerView.Adapter<PokemonViewHolder> {

    private ArrayList<Pokemon> dataset;
    private Context activity;

    public PokemonRecyclerViewAdapter(Context activity) {
        dataset = new ArrayList<>();
        this.activity = activity;
    }

    @NonNull
    @Override
    public PokemonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pokemon_api_item, parent, false);
        return new PokemonViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull PokemonViewHolder holder, int position) {
        Pokemon p = dataset.get(position);
        holder.getPokemonNameView().setText(p.getName());

        Glide.with(activity).load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + p.getNumber() + ".png")
                            //.centerCrop()
                            //.crossFade()
                            .into(holder.getPokemonImageView());

    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void addToDataset(ArrayList<Pokemon> results) {
        dataset.addAll(results);
        notifyDataSetChanged();
    }
}
