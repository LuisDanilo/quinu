package com.juaji.quinu.PokemonAPI;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.juaji.quinu.R;

public class PokemonViewHolder extends RecyclerView.ViewHolder {

    private ImageView pokemonImageView;
    private TextView pokemonNameView;

    public PokemonViewHolder(@NonNull View itemView) {
        super(itemView);
        pokemonImageView = itemView.findViewById(R.id.pokemonImage);
        pokemonNameView = itemView.findViewById(R.id.pokemonName);
    }

    public ImageView getPokemonImageView() {
        return pokemonImageView;
    }

    public void setPokemonImageView(ImageView pokemonImage) {
        this.pokemonImageView = pokemonImage;
    }

    public TextView getPokemonNameView() {
        return pokemonNameView;
    }

    public void setPokemonNameView(TextView pokemonName) {
        this.pokemonNameView = pokemonName;
    }
}
