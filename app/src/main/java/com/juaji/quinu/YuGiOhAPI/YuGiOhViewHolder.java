package com.juaji.quinu.YuGiOhAPI;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.juaji.quinu.R;

class YuGiOhViewHolder extends RecyclerView.ViewHolder {

    private ImageView cardImageView;
    private TextView cardNameView;

    public YuGiOhViewHolder(@NonNull View itemView) {
        super(itemView);
        cardImageView = itemView.findViewById(R.id.yugiohCardImageView);
        cardNameView = itemView.findViewById(R.id.yugiohCardNameView);
    }

    public ImageView getCardImageView() {
        return cardImageView;
    }

    public void setCardImageView(ImageView cardImageView) {
        this.cardImageView = cardImageView;
    }

    public TextView getCardNameView() {
        return cardNameView;
    }

    public void setCardNameView(TextView cardNameView) {
        this.cardNameView = cardNameView;
    }
}
