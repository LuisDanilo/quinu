package com.juaji.quinu.YuGiOhAPI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.juaji.quinu.R;

import java.util.ArrayList;
import java.util.List;

class YuGiOhRecyclerViewAdapter extends RecyclerView.Adapter<YuGiOhViewHolder> {

    private Context activity;
    private ArrayList<YuGiOhCard> dataset;

    public YuGiOhRecyclerViewAdapter(Context activity) {
        dataset = new ArrayList<>();
        this.activity = activity;
    }

    @NonNull
    @Override
    public YuGiOhViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.yugioh_api_item, parent, false);
        return new YuGiOhViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull YuGiOhViewHolder holder, int position) {
        YuGiOhCard c = dataset.get(position);
        holder.getCardNameView().setText(c.getName());

        Glide.with(activity).load(c.getImage())
                //.centerCrop()
                //.crossFade()
                .into(holder.getCardImageView());
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void addToDataset(List<YuGiOhCard> body) {
        dataset.addAll(body);
        notifyDataSetChanged();
    }
}
