package com.juaji.quinu.YuGiOhAPI;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.juaji.quinu.ApiFragment;
import com.juaji.quinu.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class YuGiOhApiFragment extends ApiFragment {

    private YuGiOhRecyclerViewAdapter recyclerViewAdapter;

    // -------------- Métodos --------------------

    /**
     * Constructor de la clase.
     *
     * @param baseUrl String con la url base de la API a consumir.
     */
    public YuGiOhApiFragment(String baseUrl, String complementaryUrl) {
        super(baseUrl, complementaryUrl);
    }

    /**
     * Método que realiza la obtención de datos del API requerida.
     * A pesar de que cada fragmento hace uso de una instancia de ApiService, esto no impide
     * que desde este fragmento se pueda obtener datos de una API que no corresponde al mismo.
     */
    private void getData() {
        // Este fragmento es para la API de openMovie, por tanto se usa el método respectivo.
        Call<List<YuGiOhCard>> data = service.getYuGiOhData(complementaryUrl, "Structure Deck: Yugi Muto");
        data.enqueue(new Callback<List<YuGiOhCard>>() {
            @Override
            public void onResponse(Call<List<YuGiOhCard>> call, Response<List<YuGiOhCard>> response) {
                if (response.isSuccessful()) {
                    recyclerViewAdapter.addToDataset(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<YuGiOhCard>> call, Throwable t) {

            }
        });
    }

    /**
     * Método sobreescrito para establecer la vista asociada a este controlador
     * @param inflater No lo se
     * @param container No lo se
     * @param savedInstanceState No lo se
     * @return La vista asociada a este controlador
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.yugioh_api_fragment, container, false);
        recyclerView = rootView.findViewById(R.id.yuGiOhRecycler);
        recyclerViewAdapter = new YuGiOhRecyclerViewAdapter(getActivity());
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        getData();
        return rootView;
    }
}
