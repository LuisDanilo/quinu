package com.juaji.quinu.YuGiOhAPI;

public class YuGiOhCard {
    private String id;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return "https://storage.googleapis.com/ygoprodeck.com/pics_small/" + id + ".jpg";
    }
}
