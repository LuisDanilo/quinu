package com.juaji.quinu.OpenMovieAPI;

import java.util.ArrayList;

public class OpenMovieApiResponseHandler {
    // ----- Atributos ------
    ArrayList<OpenMovie> Search;

    // ----- Métodos --------

    public ArrayList<OpenMovie> getSearch() {
        return Search;
    }

    public void setSearch(ArrayList<OpenMovie> search) {
        Search = search;
    }
}
