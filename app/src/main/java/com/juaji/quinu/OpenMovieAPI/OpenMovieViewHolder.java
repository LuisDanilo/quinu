package com.juaji.quinu.OpenMovieAPI;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.juaji.quinu.R;

class OpenMovieViewHolder extends RecyclerView.ViewHolder {

    private ImageView openMoviePosterView;
    private TextView openMovieTitleView;
    private TextView openMovieYearView;

    public OpenMovieViewHolder(@NonNull View itemView) {
        super(itemView);
        openMoviePosterView = itemView.findViewById(R.id.openMoviePosterView);
        openMovieTitleView = itemView.findViewById(R.id.openMovieTitleView);
        openMovieYearView = itemView.findViewById(R.id.openMovieYearView);
    }

    public ImageView getOpenMoviePosterView() {
        return openMoviePosterView;
    }

    public void setOpenMoviePosterView(ImageView openMoviePosterView) {
        this.openMoviePosterView = openMoviePosterView;
    }

    public TextView getOpenMovieTitleView() {
        return openMovieTitleView;
    }

    public void setOpenMovieTitleView(TextView openMovieTitleView) {
        this.openMovieTitleView = openMovieTitleView;
    }

    public TextView getOpenMovieYearView() {
        return openMovieYearView;
    }

    public void setOpenMovieYearView(TextView openMovieYearView) {
        this.openMovieYearView = openMovieYearView;
    }
}
