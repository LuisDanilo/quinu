package com.juaji.quinu.OpenMovieAPI;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.juaji.quinu.ApiFragment;
import com.juaji.quinu.PokemonAPI.PokemonApiResponseHandler;
import com.juaji.quinu.PokemonAPI.PokemonRecyclerViewAdapter;
import com.juaji.quinu.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OpenMovieApiFragment extends ApiFragment {

    private OpenMovieRecyclerViewAdapter recyclerViewAdapter;
    private String key;

    // -------------- Métodos --------------------

    /**
     * Constructor de la clase.
     * @param baseUrl String con la url base de la API a consumir.
     */
    public OpenMovieApiFragment(String baseUrl, String complementaryUrl) {
        super(baseUrl, complementaryUrl);
        key = "f46629a1";
    }

    /**
     * Método que realiza la obtención de datos del API requerida.
     * A pesar de que cada fragmento hace uso de una instancia de ApiService, esto no impide
     * que desde este fragmento se pueda obtener datos de una API que no corresponde al mismo.
     */
    private void getData() {
        // Este fragmento es para la API de openMovie, por tanto se usa el método respectivo.
        Call<OpenMovieApiResponseHandler> data = service.getOpenMovieData(complementaryUrl, key);
        data.enqueue(new Callback<OpenMovieApiResponseHandler>() {
            @Override
            public void onResponse(Call<OpenMovieApiResponseHandler> call, Response<OpenMovieApiResponseHandler> response) {
                if (response.isSuccessful()) {
                    OpenMovieApiResponseHandler body = response.body();
                    recyclerViewAdapter.addToDataset(body.getSearch());
                }
            }

            @Override
            public void onFailure(Call<OpenMovieApiResponseHandler> call, Throwable t) {

            }
        });
    }


    /**
     * Método sobreescrito para establecer la vista asociada a este controlador
     * @param inflater No lo se
     * @param container No lo se
     * @param savedInstanceState No lo se
     * @return La vista asociada a este controlador
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.openmovie_api_fragment, container, false);
        recyclerView = rootView.findViewById(R.id.openMovieRecycler);
        recyclerViewAdapter = new OpenMovieRecyclerViewAdapter(getActivity());
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        getData();
        return rootView;
    }
}
