package com.juaji.quinu.OpenMovieAPI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.juaji.quinu.R;

import java.util.ArrayList;

class OpenMovieRecyclerViewAdapter extends RecyclerView.Adapter<OpenMovieViewHolder> {

    private ArrayList<OpenMovie> dataset;
    private Context activity;

    public OpenMovieRecyclerViewAdapter(Context activity) {
        dataset = new ArrayList<>();
        this.activity = activity;
    }

    @NonNull
    @Override
    public OpenMovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.openmovie_api_item, parent, false);
        return new OpenMovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OpenMovieViewHolder holder, int position) {
        OpenMovie m = dataset.get(position);
        holder.getOpenMovieTitleView().setText(m.getTitle());
        holder.getOpenMovieYearView().setText(m.getYear());

        Glide.with(activity).load(m.getPoster())
                //.centerCrop()
                //.crossFade()
                .into(holder.getOpenMoviePosterView());
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void addToDataset(ArrayList<OpenMovie> results) {
        dataset.addAll(results);
        notifyDataSetChanged();
    }

}
