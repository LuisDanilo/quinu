package com.juaji.quinu.ViveDigitalAPI;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.juaji.quinu.ApiFragment;
import com.juaji.quinu.R;
import com.juaji.quinu.YuGiOhAPI.YuGiOhCard;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViveDigitalApiFragment extends ApiFragment {

    private MapView mapView;
    private GoogleMap mgoogleMap;
    private List<PuntoViveDigital> puntosViveDigital;

    // -------------- Métodos --------------------

    /**
     * Constructor de la clase.
     *
     * @param baseUrl String con la url base de la API a consumir.
     */
    public ViveDigitalApiFragment(String baseUrl, String complementaryUrl) {
        super(baseUrl, complementaryUrl);
        puntosViveDigital = new ArrayList<>();
    }

    private void getData() {
        Call<List<PuntoViveDigital>> data = service.getViveDigitalData(complementaryUrl);
        data.enqueue(new Callback<List<PuntoViveDigital>>() {
            @Override
            public void onResponse(Call<List<PuntoViveDigital>> call, Response<List<PuntoViveDigital>> response) {
                if (response.isSuccessful()) {
                    puntosViveDigital.addAll(response.body());
                    for (PuntoViveDigital p : puntosViveDigital) {
                        LatLng newPos = new LatLng(p.getLatitud(), p.getLongitud());
                        mgoogleMap.addMarker(new MarkerOptions().position(newPos).title(p.getNombre_pvd()));
                        Log.i("MP", p.getNombre_pvd());
                    }
                    PuntoViveDigital first = puntosViveDigital.get(0);
                    LatLng defaultPosition = new LatLng(first.getLatitud(), first.getLongitud());
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(defaultPosition).zoom(12).build();
                    mgoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }
            }

            @Override
            public void onFailure(Call<List<PuntoViveDigital>> call, Throwable t) {
            }
        });
    }

    /**
     * Método sobreescrito para establecer la vista asociada a este controlador
     * @param inflater No lo se
     * @param container No lo se
     * @param savedInstanceState No lo se
     * @return La vista asociada a este controlador
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.vivedigital_api_fragment, container, false);
        mapView = rootView.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        }
        catch(Exception e) {
            //
        }
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mgoogleMap = googleMap;
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    public void setPuntosViveDigital(ArrayList<PuntoViveDigital> puntosViveDigital) {
        this.puntosViveDigital = puntosViveDigital;
    }
}
