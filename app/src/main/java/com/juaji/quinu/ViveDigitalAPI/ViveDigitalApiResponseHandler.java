package com.juaji.quinu.ViveDigitalAPI;

import java.util.List;

public class ViveDigitalApiResponseHandler {

    private List<PuntoViveDigital> puntosViveDigital;

    public List<PuntoViveDigital> getPuntosViveDigital() {
        return puntosViveDigital;
    }

    public void setPuntosViveDigital(List<PuntoViveDigital> puntosViveDigital) {
        this.puntosViveDigital = puntosViveDigital;
    }
}

