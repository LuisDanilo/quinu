package com.juaji.quinu.ViveDigitalAPI;

public class PuntoViveDigital {

    private String nombre_pvd;
    private String longitud;
    private String latitud;

    public String getNombre_pvd() {
        return nombre_pvd;
    }

    public void setNombre_pvd(String nombre_pvd) {
        this.nombre_pvd = nombre_pvd;
    }

    public float getLongitud() {
        longitud.replace(",", ".");
        return Float.parseFloat(longitud);
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public float getLatitud() {
        latitud.replace(",", ".");
        return Float.parseFloat(latitud);
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }
}
