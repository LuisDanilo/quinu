package com.juaji.quinu;

import com.juaji.quinu.OpenMovieAPI.OpenMovieApiResponseHandler;
import com.juaji.quinu.PokemonAPI.PokemonApiResponseHandler;
import com.juaji.quinu.ViveDigitalAPI.PuntoViveDigital;
import com.juaji.quinu.YuGiOhAPI.YuGiOhCard;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Interfaz que será instanciada por retrofit y por la cual iniciará la obtención de datos.
 * Se definieron varios métodos, cada uno sera invocado en el controlador de fragmento respectivo.
 */
public interface ApiService {
    @GET
    Call<OpenMovieApiResponseHandler> getOpenMovieData(@Url String complementaryUrl, @Query("apikey") String apiKey);
    @GET
    Call<PokemonApiResponseHandler> getPokemonData(@Url String complementaryUrl/*, @Query("limit") byte limit, @Query("offset") byte offset*/);
    @GET
    Call<List<PuntoViveDigital>> getViveDigitalData(@Url String complementaryUrl);
    @GET
    Call<List<YuGiOhCard>> getYuGiOhData(@Url String complementaryUrl, @Query("set") String cardSet);
}
