package com.juaji.quinu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;

import com.google.android.material.tabs.TabLayout;
import com.juaji.quinu.OpenMovieAPI.OpenMovieApiFragment;
import com.juaji.quinu.PokemonAPI.PokemonApiFragment;
import com.juaji.quinu.TempoAPI.TempoApiFragment;
import com.juaji.quinu.ViveDigitalAPI.ViveDigitalApiFragment;
import com.juaji.quinu.YuGiOhAPI.YuGiOhApiFragment;

public class MainActivity extends AppCompatActivity {

    // ------------------ Atributos -------------------

    // TabLayout, ViewPager y ViewPagerAdapter
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;

    // ----------------- Métodos ----------------------

    /**
     * Método que permite inicializar el tabLayout según las configuraciones hechas en el viewPager
     */
    private void setupTabLayout() {
        tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);

        //TODO Personalizar tabs con un icono y/o un titulo

        // Nombres termporales a las tabs
        for (int i = 0; i < viewPagerAdapter.getCount(); i -=- 1) {
            switch(i) {
                case 0:
                    tabLayout.getTabAt(i).setText(R.string.pokemon_tab);
                    break;
                case 1:
                    tabLayout.getTabAt(i).setText(R.string.openmovie_tab);
                    break;
                case 2:
                    tabLayout.getTabAt(i).setText(R.string.vivedigital_tab);
                    break;
                case 3:
                    tabLayout.getTabAt(i).setText(R.string.yugioh_tab);
                    break;
                default: tabLayout.getTabAt(i).setText("Tab " + i);
            }
        }
    }

    /**
     * Método que permite inicializar el viewPager, con el cual de dispone de los fragments
     * como si fuesen páginas de un libro.
     */
    private void setupViewPager() {
        viewPager = findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        // Agregar fragments
        viewPagerAdapter.addFragment(new PokemonApiFragment("https://pokeapi.co/api/v2/", "pokemon?limit=30"));
        //viewPagerAdapter.addFragment(new TempoApiFragment("https://.com/", ""));
        viewPagerAdapter.addFragment(new OpenMovieApiFragment("http://omdbapi.com/", "?s=Star+Wars"));
        viewPagerAdapter.addFragment(new ViveDigitalApiFragment("https://www.datos.gov.co/resource/", "etr2-mkeu.json"));
        viewPagerAdapter.addFragment(new YuGiOhApiFragment("http://db.ygoprodeck.com/api/v5/cardinfo.php/", ""));
        viewPager.setAdapter(viewPagerAdapter);
    }

    /**
     * Método en el que inicio a programar, de esta manera se que gran parte de mi trabajo
     * se encuentra a partir de este punto, separándolo del código que el IDE genera.
     */
    private void init() {
        // Inicializar viewPager
        setupViewPager();
        // Inicializar tabLayout
        setupTabLayout();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }
}
