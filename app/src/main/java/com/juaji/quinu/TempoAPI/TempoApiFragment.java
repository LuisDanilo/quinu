package com.juaji.quinu.TempoAPI;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.juaji.quinu.ApiFragment;
import com.juaji.quinu.R;


public class TempoApiFragment extends ApiFragment {

    // ------------------ Métodos ------------------

    /**
     * Constructor de la clase.
     *
     * @param baseUrl String con la url base de la API a consumir.
     */
    public TempoApiFragment(String baseUrl, String complementaryUrl) {
        super(baseUrl, complementaryUrl);
    }

    /**
     * Método sobreescrito para establecer la vista asociada a este controlador
     * @param inflater No lo se
     * @param container No lo se
     * @param savedInstanceState No lo se
     * @return La vista asociada a este controlador
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.tempoapi_fragment, container, false);
        return rootView;
    }
}
