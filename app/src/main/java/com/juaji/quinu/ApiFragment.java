package com.juaji.quinu;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Clase que implementa un controlador de fragmento con atributos y métodos adicionales.
 * Además por medio de la misma se realiza la obtención de datos de la API designada, esto
 * con el fin de actualizar la vista según los datos obtenidos.
 */
public abstract class ApiFragment extends Fragment {

    // ------------- Atributos ------------------

    // Vista a referenciar
    protected View rootView;
    // Url base de la API (endpoint) y retrofit
    protected String baseUrl;
    protected String complementaryUrl;
    protected Retrofit retrofit;
    protected ApiService service;
    // RecyclerView, GridLayoutManager
    protected RecyclerView recyclerView;

    // --------------- Métodos --------------------

    /**
     * Constructor de la clase.
     * @param baseUrl String con la url base de la API a consumir.
     * @param complementaryUrl String con la url complementaria de la API a consumir.
     */
    public ApiFragment(String baseUrl, String complementaryUrl) {
        this.baseUrl = baseUrl;
        this.complementaryUrl = complementaryUrl;
        // Inicializar retrofit con la url base dada
        retrofit = new Retrofit.Builder()
                               .baseUrl(baseUrl)
                               .addConverterFactory(GsonConverterFactory.create())
                               .build();
        // Intanciar ApiService
        service = retrofit.create(ApiService.class);
    }
}
